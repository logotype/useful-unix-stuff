#!/usr/bin/env bash
sudo echo ""

echo -e "\e[92mUPDATE PACKAGE SOURCES \e[39m"
sudo apt-get update
echo -e "\e[92mUPGRADE TO NEW PACKAGES \e[39m"
sudo apt-get upgrade
echo -e "\e[92mUPGRADE DISTRIBUTION-PACKAGES \e[39m"
sudo apt-get dist-upgrade
echo -e "\e[92mAUTOREMOVE OLD PACKAGES \e[39m"
sudo apt-get autoremove