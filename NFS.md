Shared NFS drive (Ubuntu 16.04 LTS)
===================================

Server
------

Install services (on the server):

```bash
$ sudo apt-get install nfs-kernel-server
$ sudo systemctl enable nfs-kernel-server.service
$ sudo systemctl start nfs-kernel-server.service
```

Edit `/etc/exports`:

```bash
/srv/nfs4               10.0.1.0/24(rw,async,no_subtree_check,crossmnt,fsid=0,all_squash)
/srv/nfs4/<name1>       10.0.1.0/24(rw,async,insecure,no_subtree_check,all_squash,anonuid=1000,anongid=1000)
/srv/nfs4/<name2>       10.0.1.0/24(rw,async,insecure,no_subtree_check,all_squash,anonuid=1000,anongid=1000)
```

Verify configuration:

```bash
$ exportfs -rv
$ exportfs -a
$ sudo systemctl restart nfs-kernel-server.service
```

Edit `/etc/fstab`:

```bash
/dev/sdb /srv/nfs4/<name1> auto nosuid,nodev,nofail,x-gvfs-show 0 0
```

Clients
-------

Install services (on each client):

```bash
$ sudo apt install nfs-common
```

Mount the drive:

```bash
$ sudo mkdir /nfsdisk
$ sudo mount <SERVER-IP>:/nfsdisk /nfsdisk
```

Mount the drive at boot, edit `/etc/fstab`:

```bash
<SERVER-IP>:/nfsdisk /nfsdisk nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```
